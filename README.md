##Please run Commands

- composer install 

- get env.example and make .env.

- make database in phpmyadmin.

- php artisan key:generate --ansi

- php artisan migrate --seed

- php artisan storage:link

- npm install

- npm run dev

- php artisan serve

Note: Login as default place-holder email and password to navigate the dashboard. 

