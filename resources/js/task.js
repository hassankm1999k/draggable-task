import Vue from 'vue';

import TableDraggableComponent from './components/TableDraggableComponent';


const tableDraggableComponent = new Vue({
    el: '#tableDraggableComponent',
    components: {
        TableDraggableComponent
    },
    name: 'TableDraggableComponent'
});
