<div class="table-responsive py-4">
    <form action="{{ route('task.index') }}">
        <div class="row ml-3">
            <div class="col-4">
                <x-select :required="false" displayName="name" :name="'project_id'" :options="getProjects()" :oldValue="$entity ?? null" ></x-select>
            </div>
            <div class="col-2">
                <button type="submit" class="btn btn-primary">Search</button>
            </div>
        </div>
    </form>

    <table-draggable-component id="tableDraggableComponent" :tasks="{{ $tasks }}"></table-draggable-component>
</div>


