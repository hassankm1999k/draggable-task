<x-text :name="'name'" :oldValue="$entity ?? null" :required="true"></x-text>
<x-number :name="'priority'" :oldValue="$entity ?? null" :required="true"></x-number>
<x-select :required="true" displayName="name" :name="'project_id'" :options="getProjects()" :oldValue="$entity->project ?? null" ></x-select>

