<div class="table-responsive py-4">
    <table class="table align-items-center table-flush"  id="datatable-basic">
        <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">{{ __('Name') }}</th>
            <th scope="col">{{ __('Created At') }}</th>
            <th scope="col">{{ __('Actions') }}</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($projects as $project)
            <tr>
                <td>
                     {{ $loop->index + 1 }}
                </td>
                <td>{{ $project->name }}</td>

                @if($project->created_at)
                    <td>{{ $project->created_at->format('d/m/Y H:i') }}</td>
                @endif

                <td class="table-actions">
                    <a href="{{ route('project.edit', $project) }}" class="table-action" data-toggle="tooltip" data-original-title="Edit Project">
                        <i class="fas fa-user-edit"></i>
                    </a>

                    <form action="{{ route('project.destroy', $project) }}" method="post" style="display: inline-block">
                        @csrf
                        @method('delete')
                        <a href="#!" class="table-action" data-toggle="tooltip" data-original-title="Delete Project" onclick="confirm('{{ __("Are you sure you want to delete this project?") }}') ? this.parentElement.submit() : ''">
                            <i class="fas fa-trash"></i>
                        </a>
                    </form>
                </td>
            </tr>
        @endforeach
        @if($projects == null)
            <p class="text-center mt-5">There is no data to show.</p>
        @endif
        </tbody>
    </table>

</div>
