<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner scroll-scrollx_visible">
        <div class="sidenav-header d-flex align-items-center">
            <a class="navbar-brand" href="{{ route('home') }}">
                <h1 class="font-weight-700 header">Draggable</h1>
            </a>
            <div class="ml-auto mb-2">
                <!-- Sidenav toggler -->
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="navbar-inner mb-4">
            <!-- Collapse -->
            <div id="sidenav-collapse-main">
                <!-- Nav items -->
                <i class="ni ni-shop text-primary"></i>
                <a  href="{{ route('project.index','Project') }}" class="nav-link" style="display: inline-block">{{ __('List Projects') }}</a>
            </div>
        </div>

        <div class="navbar-inner mb-4">
            <!-- Collapse -->
            <div id="sidenav-collapse-main">
                <!-- Nav items -->
                <i class="ni ni-shop text-primary"></i>
                <a  href="{{ route('task.index','Task') }}" class="nav-link" style="display: inline-block">{{ __('List Tasks') }}</a>
            </div>
        </div>
    </div>
</nav>
