<div class="form-group row">
    <label class="col-xl-3 col-lg-3 col-form-label @if($required) required @endif" for="{{ $name }}">
        {{ isset($item) ? __($item.'.'.$name) : __('admin.'.$name) }}
        @if($required)
            <span class="text-red"> * </span>
        @endif
    </label>
    <div class="col-lg-9 col-xl-9">
    <select class="form-control m-bot15 select2 @error($name) is-invalid @enderror"
            id="{{ $name }}" name="{{ $name }}{{$multiple?'[]':''}}"
            {{ $multiple ? 'multiple="multiple"' : '' }}
            {{ $required ? 'data-parsley-trigger="change" required' : '' }}
            style="width: 100% !important; opacity: 1 !important;">
        @if($all == true)
            <option {{ $oldValue ? '' : 'selected' }} value="0"> {{ __('admin.all_'.plural($name)) }}</option>
        @endif
        @if($multiple == false)
            <option disabled value="0" {{!$oldValue ? 'selected' : ''}}>{{ __('admin.empty') }}</option>
            @foreach($options as $option)
                @php $option = json_decode(json_encode($option)) @endphp
                <option {{  $oldValue ? (is_object($oldValue) ? ($option->id == $oldValue->id  ? 'selected' : '') : ($option->id == $oldValue ? 'selected' : '')) : (old($name) == $option->id ? 'selected' : '') }} value="{{ $option->id }}">{{  $option->$displayName }}</option>
            @endforeach
        @else
            @foreach($options as $option)
                @php $option = json_decode(json_encode($option)) @endphp
                <option {{ $oldValues ? ( in_array($option->id, $oldValues) ? 'selected' : '') : '' }} value="{{ $option->id }}">{{$option->$displayName }}</option>
            @endforeach
        @endif
    </select>
    </div>
    @error($name)
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror


</div>
