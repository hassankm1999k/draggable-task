<div class="container-fluid mt-4">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card">
{{--                <div class="card-header">--}}
{{--                    <div class="row align-items-center">--}}
{{--                        <div class="col-8">--}}
{{--                            <h3 class="mb-0">{{ __('User Management') }}</h3>--}}
{{--                        </div>--}}
{{--                        <div class="col-4 text-right">--}}
{{--                            <a href="{{ route('user.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="card-body">
                    <form data-parsley-validate method="post" action="{{ $action ?? '#' }}" autocomplete="off" enctype="multipart/form-data">
                        @csrf

                        @if(isset($entity))
                            @method('PUT')
                        @endif
                        <h1 class="mt-10" >{{ __('admin.'.getAction())  }}  @if(isset(${$item}))<code>{{ ${$item}->name }}</code>@endif {{ __('admin.'.$item) }}</h1>
                        <div class="row">
                            <div class="col-8">
                                <div class="pl-lg-4">

                                    @include($item . '._form')

                                    <button type="submit" class="btn btn-primary mr-2">{{ $submit == 'PUT' ? __('Update') : __('Save') }}</button>
                                    <button type="submit" class="btn btn-secondary" name="add-new">{{ $submit == 'PUT' ? __('Update And Stay') : __('save and new') }} </button>

                                </div>
                            </div>


                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footers.auth')
</div>
