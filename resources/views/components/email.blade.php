<div class="form-group row">
    <label class="col-xl-3 col-lg-3 col-form-label">
        {{ isset($item) ? __($item.'.'.$name) : __('admin.'.$name) }}
        @if($required)
            <span class="text-red"> * </span>
        @endif
    </label>

    <div class="col-lg-9 col-xl-9">
        <input type="email" class="form-control form-control-sm mt-2 @error($name) is-invalid @enderror"
               placeholder="{{ __('admin.enter') }} {{ isset($item) ? __($item.'.'.$name) : __('admin.'.$name)}}"
               name="{{ $name }}"
               value="{{ $oldValue ? $oldValue->{$name} : old($name) }}"
        />
    </div>
    @error($name)
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>
