@extends('layouts.app', [
    'title' => __(plural($item).' Management'),
    'parentSection' => 'laravel',
    'elementName' => plural($item).'-management'
])
@push('css')
    <link href="{{asset('argon/libs/dropify/dropify.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('argon/css/app.min.css')}}" rel="stylesheet" type="text/css"  id="app-stylesheet" />

@endpush
@section('content')
    @component('layouts.headers.auth')
        @component('layouts.headers.breadcrumbs')
            @slot('title')
                {{ __('Examples') }}
            @endslot

            <li class="breadcrumb-item"><a href="{{ route($item.'.index') }}">{{ __($item.' Management') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ __('Add '. $item) }}</li>
        @endcomponent
    @endcomponent

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __($item.' Management') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route($item.'.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-2">
                        @include('alerts.success')
                        @include('alerts.errors')
                    </div>
                    <div class="card-body">
                        <x-form :action="getNextAction($item, ${$item} ?? null)" :entity="${$item} ?? null" :submit="getActionMethod()"></x-form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <!-- Plugins js -->
    <script src="{{asset('argon/libs/dropify/dropify.min.js')}}"></script>

    <!-- Init js-->
    <script src="{{asset('argon/js/pages/form-fileuploads.init.js')}}"></script>


    <script src="{{asset('argon/libs/parsleyjs/parsley.min.js')}}"></script>

    <!-- Validation init js-->
    <script src="{{asset('argon/js/pages/form-validation.init.js')}}"></script>

@endpush
