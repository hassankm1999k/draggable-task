<?php

namespace Database\Seeders;

use App\Models\Project;
use App\Models\Task;
use Illuminate\Database\Seeder;
use Faker\Generator;

class TaskSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $projects = Project::all();

        $counter = 0;
        foreach ($projects as $project){
            for( $i=0 ; $i<4; $i++){
                $counter++;
                Task::create([
                    'name' => 'Task' . $counter ,
                    'priority' => $counter,
                    'project_id' => $project->id
                ]);
            }
        }
    }
}
