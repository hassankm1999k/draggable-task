<?php

namespace Database\Seeders;

use App\Models\Project;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $projects = ['Laravel', 'Symfony', 'Node-Js', 'Ruby'];
        foreach ($projects as $project){
            Project::create([
                'name' => $project
            ]);
        }
    }
}
