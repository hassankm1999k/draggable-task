<?php

use Illuminate\Support\Str;

function camel($string = ''): string
{
    return Str::camel($string);
}

function plural($string): string
{
    return Str::plural($string);
}

function toTitle($string = ''): \Illuminate\Support\Stringable
{
    return Str::of($string)->replace('_', ' ')->replaceMatches('/\d+/u',' ')->ucfirst();
}

