<?php

use Illuminate\Support\Facades\Route;
use App\Models\Project;

function getAction(): ?string
{
    return explode("@", Route::currentRouteAction())[1] ?? "";
}

function getActionMethod(): ?string
{
    $currentAction = explode("@", Route::currentRouteAction())[1];
    if ($currentAction == 'create')
        return 'POST';
    else
        return 'PUT';
}

function getNextAction($item, $entity = null): string
{
    $currentAction = explode("@", Route::currentRouteAction())[1];
    if ($currentAction == 'create')
        return route( $item .'.store');
    else{

        return route($item .'.update', [strval($item) => $entity]);
    }

}

function getProjects()
{
    return Project::all();
}
