<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskRequestForm;
use App\Models\Project;
use App\Models\Task;
use App\Repositories\TaskRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskController extends Controller
{
    private $taskRepository;
    private $resource = 'task';
    public function __construct(TaskRepository $taskRepository)
    {
        view()->share('item', $this->resource);
        $this->taskRepository = $taskRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View|Application|Factory|View
     */
    public function index(Request $request)
    {
        if ($request->project_id)
            $entity = Project::where('id', $request->project_id)->first();
        else
            $entity = null;
        $tasks = $this->taskRepository->getAll($request);

        return \view('crud.index', compact('tasks', 'entity'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        return view('crud.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TaskRequestForm $request
     * @return RedirectResponse
     */
    public function store(TaskRequestForm $request)
    {
        $this->taskRepository->create($request->all());

        if ($request->has('add-new'))
            return redirect()->route('task.create')->withStatus(__('Task successfully Created.'));

        return redirect()->route('task.index')->withStatus(__('Task successfully created.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Task $task
     * @return Application|Factory|Response|View
     */
    public function edit(Task $task)
    {
        return view('crud.create-edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TaskRequestForm $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(TaskRequestForm $request ,$id)
    {
        $this->taskRepository->update($id,$request->all());

        if ($request->has('add-new'))
            return redirect()->route('task.edit',$id)->withStatus(__('Task successfully Updated.'));

        return redirect()->route('task.index')->withStatus(__('Task successfully Updated.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $this->taskRepository->delete($id);

        return redirect()->route('task.index')->withStatus(__('Task successfully deleted.'));
    }

    public function updateAll(Request $request)
    {
        foreach ($request->tasks as $task){
            $newTask = Task::where('id', $task['id'])->first();
            $newTask->priority = $task['priority'];
            $newTask->save();
        }

        //Returning any response depends on the requirements.
        return \response()->json(
            'Ordering..'
        );
    }
}
