<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectRequestForm;
use App\Models\Project;
use App\Repositories\ProjectRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProjectController extends Controller
{
    private $projectRepository;
    private $resource = 'project';
    public function __construct(ProjectRepository $projectRepository)
    {
        view()->share('item', $this->resource);
        $this->projectRepository = $projectRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View|Application|Factory|View
     */
    public function index(Request $request)
    {
        $projects = $this->projectRepository->getAll($request);
        return \view('crud.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        return view('crud.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProjectRequestForm $request
     * @return RedirectResponse
     */
    public function store(ProjectRequestForm $request)
    {
        $this->projectRepository->create($request->all());

        if ($request->has('add-new'))
            return redirect()->route('project.create')->withStatus(__('Project successfully created.'));

        return redirect()->route('project.index')->withStatus(__('Project successfully created.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Project $project
     * @return Application|Factory|Response|View
     */
    public function edit(Project $project)
    {
        return view('crud.create-edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProjectRequestForm $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(ProjectRequestForm $request ,$id)
    {
        $this->projectRepository->update($id,$request->all());

        if ($request->has('add-new'))
            return redirect()->route('project.edit',$id);

        return redirect()->route('project.index')->withStatus(__('Project successfully Updated.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $this->projectRepository->delete($id);

        return redirect()->route('project.index')->withStatus(__('Project successfully deleted.'));
    }
}
