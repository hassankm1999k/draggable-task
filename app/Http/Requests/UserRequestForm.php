<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequestForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() :array
    {
        switch ($this->method()){
            case 'POST':
                return [
                    'name' => [
                        'required', 'min:4','string'
                    ],
                    'email' => [
                        'required',Rule::unique('user','email'),
                    ],
                    'image' => [
                        'nullable', 'image','mimes:jpeg,jpg,png'
                    ],
                    'password' => [
                        'required', 'confirmed','min:6'
                    ],
                ];
            case 'PUT':
                return [
                    'name' => [
                        'required', 'min:4','string'],
                    'email' => [
                        'required',Rule::unique('user','email')->ignore($this->route()->user ?? null),
                    ],
                    'image' => [
                        'nullable', 'image','mimes:jpeg,jpg,png'
                    ],
                    'password' =>[
                        'nullable','confirmed', 'min:6'
                    ],
                ];
        }
        return [];
    }
}
