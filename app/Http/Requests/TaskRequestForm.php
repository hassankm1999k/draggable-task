<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TaskRequestForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() :array
    {
        switch ($this->method()){
            case 'PUT':
            case 'POST':
                return [
                    'name' => [
                        'required', 'min:4','string'
                    ],
                    'priority' => [
                        'required'
                    ],
                    'project_id' => [
                        'required', Rule::exists('projects', 'id')
                    ],
                ];
        }
        return [];
    }
}
