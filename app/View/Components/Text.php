<?php

namespace App\View\Components;


use Illuminate\View\View;

class Text extends BaseComponent
{

    /**
     * @var boolean
     */
    public $readonly;

    public function __construct($name = '', $required = false, $oldValue='', $readonly=false, $valueName='')
    {
        parent::__construct($name, $required, $oldValue, $valueName);
        $this->readonly = $readonly;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('components.text');
    }
}
