<?php

namespace App\Repositories\Contracts;

use Illuminate\Http\Request;

interface RepositoryInterface
{
    public function getAll(Request $request);
    public function getById($id);
    public function create(array $attributes);
    public function update($id, array $attributes);
    public function delete($id);
}
