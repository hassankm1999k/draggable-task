<?php

namespace App\Repositories;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskRepository extends Repository
{
    protected $model;

    public function __construct(Task $model)
    {
        $this->model = $model;
    }

    public function getAll(Request $request)
    {
        $tasks = Task::query()->with(['project']);
        if ($request->project_id)
            $tasks->where('project_id', $request->project_id);
        return $tasks->orderBy('priority')->get();
    }
}
