<?php

namespace App\Repositories;

use App\Models\Project;

class ProjectRepository extends Repository
{
    protected $model;

    public function __construct(Project $model)
    {
        $this->model = $model;
    }
}
